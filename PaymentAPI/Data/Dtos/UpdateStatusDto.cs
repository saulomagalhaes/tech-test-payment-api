﻿namespace PaymentAPI.Data.Dtos
{
    public class UpdateStatusDto
    {
        public string Status { get; set; }
    }
}
