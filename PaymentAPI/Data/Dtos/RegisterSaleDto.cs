﻿using PaymentAPI.Models;

namespace PaymentAPI.Data.Dtos
{
    public class RegisterSaleDto
    {
        public Seller Seller { get; set; }
        public List<Item> Items { get; set; }
    }
}
