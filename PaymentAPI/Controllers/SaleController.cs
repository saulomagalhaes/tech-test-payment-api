﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Data.Dtos;
using PaymentAPI.Models;
using PaymentAPI.Repositories.Interfaces;

namespace PaymentAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SaleController : ControllerBase
    {
        private readonly ISaleRepository _saleRepository ;
        private IMapper _mapper;

        public SaleController(ISaleRepository saleRepository, IMapper mapper)
        {
            _saleRepository = saleRepository ;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult RegisterSale([FromBody] RegisterSaleDto saleDto)
        {
            var newSale = _mapper.Map<Sale>(saleDto);
            newSale.Id = Guid.NewGuid().ToString();

            try
            {
                var sale = _saleRepository.RegisterSale(newSale);
                return CreatedAtAction(nameof(SearchSaleById), new {id = sale.Id }, sale);
            } catch(Exception ex)
            {
                return NotFound(new { error = ex.Message });
            }
        }

        [HttpGet("{id}")]
        public IActionResult SearchSaleById(string id)
        {
            var sale = _saleRepository.SearchSaleById(id);
            if(sale== null) return NotFound(new { error = "Id Inválido" });
            return Ok(sale);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateStatusSale(string id, [FromBody] UpdateStatusDto statusDto)
        {
            try
            {
                var sale = _saleRepository.UpdateStatusSale(id, statusDto.Status);
                return NoContent();
            } catch(Exception ex)
            {
                return NotFound(new { error = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var sales = _saleRepository.GetAll();
            return Ok(sales);
        }
    }
}
