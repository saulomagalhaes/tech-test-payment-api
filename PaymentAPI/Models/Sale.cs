﻿using PaymentAPI.Enums;

namespace PaymentAPI.Models
{
    public class Sale
    {
        public string Id { get; set; } 
        public string Status { get; set; }
        public DateTime Data { get; set; }
        public Seller Seller { get; set; }
        public List<Item> Items { get; set; }
    }
}
