﻿namespace PaymentAPI.Models
{
    public class Seller
    {
        public string Id { get; set; } 
        public string Cpf { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
