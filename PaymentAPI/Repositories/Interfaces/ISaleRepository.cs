﻿using PaymentAPI.Data.Dtos;
using PaymentAPI.Enums;
using PaymentAPI.Models;

namespace PaymentAPI.Repositories.Interfaces
{
    public interface ISaleRepository
    {
        Sale RegisterSale(Sale sale);
        Sale SearchSaleById(string id);
        Sale UpdateStatusSale(string id, string status);
        List<Sale> GetAll();
    }   
}
