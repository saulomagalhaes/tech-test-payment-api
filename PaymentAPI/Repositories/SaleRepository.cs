﻿using PaymentAPI.Enums;
using PaymentAPI.Models;
using PaymentAPI.Repositories.Interfaces;

namespace PaymentAPI.Repositories
{
    public class SaleRepository: ISaleRepository
    {
        private static List<Sale> _sales= new List<Sale>();

        public Sale RegisterSale(Sale sale)
        {
            if(sale.Items.Count== 0)
            {
                throw new Exception("Nenhum item foi informado para a venda");
            }
            sale.Status = "Aguardando Pagamento";
            sale.Data = DateTime.Now;

            _sales.Add(sale);
            return sale;
        }

        public Sale SearchSaleById(string id)
        {
            var sale = _sales.FirstOrDefault(sale => sale.Id == id);
            return sale;
        }

        public Sale UpdateStatusSale(string id,string status)
        {
            var sale = SearchSaleById(id);

            if(sale == null)
            {
                throw new Exception("Não existe venda com o ID informado");
            }

            if (!IsValidStatusTransition(sale.Status, status))
            {
                throw new Exception("Status inválido para a transição atual");
            }

            sale.Status = status;
            return sale;
        }

        private bool IsValidStatusTransition(string currentStatus, string newStatus)
        {
            switch (currentStatus)
            {
                case "Aguardando Pagamento":
                    return newStatus == "Pagamento Aprovado" || newStatus == "Cancelada";
                case "Pagamento Aprovado":
                    return newStatus == "Enviado para Transportadora" || newStatus == "Cancelada";
                case "Enviado para Transportadora":
                    return newStatus == "Delivered";
                default:
                    return false;
            }
        }

        public List<Sale> GetAll()
        {
            return _sales.ToList();
        }

    }
}
