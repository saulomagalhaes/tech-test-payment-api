﻿using AutoMapper;
using PaymentAPI.Data.Dtos;
using PaymentAPI.Models;

namespace PaymentAPI.Profiles
{
    public class SaleProfile : Profile
    {
        public SaleProfile()
        {
            CreateMap<RegisterSaleDto, Sale>();
        }
    }
}
