﻿using System.ComponentModel;

namespace PaymentAPI.Enums
{
    public enum EnumStatusSale
    {
        [Description("Pagamento Aprovado")]
        PagamentoAprovado = 1,
        [Description("Enviado para transportadora")]
        EnviadoParaTransportadora ,
        [Description("Entregue ")]
        Entregue,
        [Description("Cancelada")]
        Cancelada,
        [Description("Aguardando Pagamento")]
        AguardandoPagamento,
    }
}
